﻿using System.Threading.Tasks;

namespace Got.TelegramBot
{
    public interface ITelegramClient
    {
        /// <summary>Отправить сообщение боту</summary>
        /// <param name="message"></param>
        Task SendMessageToBotAsync(string message);
        /// <summary>Отправить сообщение боту</summary>
        /// <param name="message"></param>
        void SendMessageToBot(string message);
        /// <summary>
        /// Обновляет клиент бота.
        /// </summary>
        /// <param name="id">id группы, в которую будут отправляться сообщения.(должно быть отрицательным)</param>
        /// <param name="proxy">Адрес прокси сервера</param>
        /// <param name="host">порт прокси сервера</param>
        void UpdateClient(long id, string proxy = null, int host = 0);
    }
}
