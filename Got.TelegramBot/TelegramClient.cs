﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using MihaZupan;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types.Enums;

namespace Got.TelegramBot
{
    public class TelegramClient : ITelegramClient
    {
        private const int BOT_ID = 483295389;
        private const string API_KEY = "483295389:AAGJBoZuzRsKuJWOCGzg2WrYVuYPW7Osz1Q";

        private readonly string[] _listOfHello =
        {
            "Hello", "Aloha!", "It's again me!", "Hola, amigos!", "Меня снова запустили!",
            "Hello Comrades!", "Bonjour!", "Welcome back! Me..", "BORN TO TRADE!"
        };

        private ITelegramBotClient _botClient;
        private readonly Dictionary<long, string> _chatIds = new Dictionary<long, string>();
        private long _currentId;
        private string _helloMessage;
        private string _proxy = "47.241.16.16";
        private int _host = 1080;

        private readonly bool _isUseProxy;

        public TelegramClient(bool isUseProxy = false)
        {
            _isUseProxy = isUseProxy;
            Init(_proxy, _host);
        }

        private void Init(string proxy, int host)
        {
            try {
                if (_isUseProxy) {
                    var proxyAddress = new HttpToSocks5Proxy(proxy, host);
                    _botClient = new TelegramBotClient(API_KEY, proxyAddress);
                } else {
                    _botClient = new TelegramBotClient(API_KEY);
                }

                _botClient.Timeout = TimeSpan.FromSeconds(10);
                _botClient.OnUpdate += BotOnUpdate;
                _botClient.OnMessage += BotOnMessage;
                _botClient.StartReceiving(Array.Empty<UpdateType>());

                _helloMessage = _listOfHello[new Random().Next(_listOfHello.Length)];
            }
            catch (Exception ex) {
                Debug.WriteLine(ex.Message);
            }
        }

        /// <inheritdoc />
        public void UpdateClient(long id, string proxy = null, int host = 0)
        {
            if (_isUseProxy) {
                if (!string.IsNullOrWhiteSpace(proxy) && host > 0) {
                    _botClient.OnUpdate -= BotOnUpdate;
                    _botClient.OnMessage -= BotOnMessage;
                    _botClient.StopReceiving();
                    _proxy = proxy;
                    _host = host;
                }
            }

            Init(proxy, host);
            _currentId = id;
        }

        /// <inheritdoc />
        public async Task SendMessageToBotAsync(string message)
        {
            try {
                if (_currentId == 0) return;
                await _botClient.SendTextMessageAsync(_currentId, message);
            }
            catch (Exception ex) {
                if (ex.Message.Contains("Forbidden: bot was kicked")) {
                    await _botClient.GetUpdatesAsync(-1);
                }
            }
        }

        /// <inheritdoc />
        public void SendMessageToBot(string message)
        {
            try {
                if (_currentId == 0) return;
                _botClient.SendTextMessageAsync(_currentId, message);
            }
            catch (Exception ex) {
                if (ex.Message.Contains("Forbidden: bot was kicked")) {
                    _botClient.GetUpdatesAsync(-1);
                }
            }
        }

        private async void BotOnUpdate(object sender, UpdateEventArgs e)
        {
            try {
                var message = e.Update.Message;
                if (message == null)
                    return;
                var messageChatId = message.Chat.Id;

                if (message.Type == MessageType.ChatMembersAdded) {
                    var title = _helloMessage + " print /start for begin";
                    _ = await _botClient.SendTextMessageAsync(messageChatId, title);
                }
            }
            catch (Exception ex) {
                if (ex.Message.Contains("Forbidden: bot was kicked")) {
                    _ = await _botClient.GetUpdatesAsync(-1);
                }
            }
        }

        private async void BotOnMessage(object sender, MessageEventArgs e)
        {
            try {
                if (e.Message?.Text == null)
                    return;

                long id = e.Message.Chat.Id;

                if (id >= 0) {
                    _ = await _botClient.SendTextMessageAsync(id, "Please, add me to Group Chat");
                    return;
                }

                var valueTitle = e.Message.Chat.Title;

                switch (e.Message.Text.ToLower()) {
                    case "/start@lucky_bff_bot":
                    case "/start":
                        if (!_chatIds.ContainsKey(id)) {
                            _chatIds.Add(id, valueTitle);

                            string title = $"Dear Traders!\n Add your Group Id {id} to Configuration!";
                            _ = await _botClient.SendTextMessageAsync(id, InfoMessageToGroup(title));
                        }

                        break;
                    case "/stop@lucky_bff_bot":
                    case "/stop":
                        if (_chatIds.ContainsKey(id)) {
                            _chatIds.Remove(id);
                        }

                        await _botClient.LeaveChatAsync(id);
                        break;
                    case "/info@lucky_bff_bot":
                    case "/info":
                        var botName = _botClient.GetMeAsync().Result.FirstName;
                        _ = await _botClient.SendTextMessageAsync(id, $"My friend, {botName} with you.\n" +
                                                                      $"current proxy: {_proxy}:{_host}\n");
                        break;
                    case "/help@lucky_bff_bot":
                    case "/help":
                        _ = await _botClient.SendTextMessageAsync(id, InfoMessageToGroup("Hope it's help to you:"));
                        break;
                }
            }
            catch (Exception ex) {
                if (ex.Message.Contains("Forbidden: bot was kicked")) {
                    _ = await _botClient.GetUpdatesAsync(-1);
                }
            }
        }

        private static string InfoMessageToGroup(string title)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(title);
            stringBuilder.AppendLine();
            stringBuilder.Append("I have a next commands: /stop, /info, /help");
            stringBuilder.AppendLine();
            stringBuilder.Append("Write /stop, if you want kick me from this group");
            stringBuilder.AppendLine();
            stringBuilder.Append("Write /info, if you want to check me");
            return stringBuilder.ToString();
        }
    }
}